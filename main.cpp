#include <fstream>

double h_max = 6;
double h_add = 0.01;
double gs_gate = 0.9;
int size = 0;
int n = 0;

struct GEM
{
    uint64_t G = 0;
    double E = 0;
    int M = 0;
};

int gs_search(GEM *gem, double h);

int main()
{
    GEM *gem;
    std::ifstream file_input("data/gem_out.dat");
    file_input >> n;
    file_input >> size;
    gem = new GEM [size];
    for(auto i=0; i<size; ++i)
    {
        file_input >> gem[i].G;
        file_input >> gem[i].E;
        file_input >> gem[i].M;
    }
    std::ofstream file_out("data/Ggs_h.dat");
    double h = 0;
    while(h < h_max)
    {
        uint64_t count = 0;
        int gs = gs_search(gem, h);
        for(auto i = 0; i < size; ++i)
        {
            if(gem[i].E - h * gem[i].M <= gem[gs].E - h * gem[gs].M + gs_gate)
            {
                count += gem[i].G;
            }
        }
        file_out << count << " " << h << "\n";
        h += h_add;
    }
}

int gs_search(GEM *gem, double h)
{
    int gs = 0;
    double Egs = 1000;
    for(int i=0; i<size; ++i)
    {
        double E = gem[i].E - h * gem[i].M;
        if(E < Egs)
        {
            Egs = E;
            gs = i;
        }
    }
    return gs;
}